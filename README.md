# README #

DbScripter is a tool to script a Sql server database into individual files for each object. These files are placed in a directory hierarchy that matches the hierarchy of folders displayed by SSMS. The purpose of this program is to ensure that the script outputs are consistent, and so that any variations of script reflect a genuine change to the database object. The scripts created can then be targeted at folders within a git repository - so, any database changes will generate a change that can be committed to the git.

Simple "linting" is also performed on the output scripts; specifically, deleting more than 2 consecutive empty lines, and removing trailing white space. Again, the purpose is to eliminate "false positives" when comparing database scripts.

Enter the server and database into the app config file. When the program runs, you can enter a user name/password to log in; or else leave the user name blank to use Windows Authentication.
