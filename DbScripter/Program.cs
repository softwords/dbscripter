﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.SqlEnum;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;
using CommandLine;

namespace DbScripter
{
	class Program
	{
		static void Main(string[] args)
		{
			CommandLine.Parser.Default.ParseArguments<Options>(args)
			  .WithParsed(RunOptions)
			  .WithNotParsed(HandleParseError);
		}

		static void HandleParseError(IEnumerable<Error> errs)
		{
		}
		static void RunOptions(Options cmdlineoptions)
		{
			string scriptRoot = ConfigurationManager.AppSettings["scriptRoot"];
			string dbName = ConfigurationManager.AppSettings["database"];

			Server srv = new Server();
			if (ConfigurationManager.AppSettings["loginSecure"] != null &&
				ConfigurationManager.AppSettings["loginSecure"].ToLower() == "true")
				srv.ConnectionContext.LoginSecure = true;
			else
			{
				srv.ConnectionContext.LoginSecure = false;
				string login = ConfigurationManager.AppSettings["login"];
				if (login == null)
				{
					Console.WriteLine("Enter user name, or leave blank to log in using Windows Authentication:");
					login = Console.ReadLine();
				}
				if (String.IsNullOrEmpty(login))
				{
					srv.ConnectionContext.LoginSecure = true;
				}
				else
				{
					srv.ConnectionContext.Login = login;
					string pwd = ConfigurationManager.AppSettings["password"];
					if (pwd == null)
					{
						Console.WriteLine("Enter Password:");
						pwd = ConsoleTools.ReadPassword();
					}
					srv.ConnectionContext.Password = pwd;
				}
			}

			if (cmdlineoptions.StoredProcs || cmdlineoptions.Views || cmdlineoptions.Tables)
			{
				cmdlineoptions.All = false;
			}
			if (cmdlineoptions.All)
			{
				cmdlineoptions.StoredProcs = true;
				cmdlineoptions.Tables = true;
				cmdlineoptions.Views = true;
				cmdlineoptions.Functions = true;
			}


			srv.ConnectionContext.ServerInstance = ConfigurationManager.AppSettings["server"];
			Database db = srv.Databases[dbName];


			ScriptingOptions options = new ScriptingOptions();
			options.Default = true;         // TRUE means script the CREATE statement for the object
			options.DriAll = true;          // all declarative referential integrity objects (foreign keys, constraints etc)
			options.Indexes = true;         // all indexes, clustered or not
			options.NoCollation = true;     // leave out collation and assume its always database_default, which should be Latin1General_CI_AS
			options.Permissions = true;             // want to script out permissions on the object
			options.IncludeDatabaseContext = false; // if TRUE, adds a USE <database> statement
			options.IncludeHeaders = false;         // the header would include a datetime stamp, which will cause git to see a change - defeats the purpose
			options.AllowSystemObjects = false;     // only want to script user objects; particularly important for views
			options.Triggers = true;                // get any triggers on tables and views
			options.ExtendedProperties = true;      // get any descriptions associated to columns
			options.ScriptOwner = false;            // if true, will generate an ALTER AUTHORIZATION statement
			options.Encoding = Encoding.ASCII;      // for easier text compare
			options.SchemaQualify = true;           // must have the schema names

			options.TargetDatabaseEngineEdition = Microsoft.SqlServer.Management.Common.DatabaseEngineEdition.Standard;
			options.TargetDatabaseEngineType = Microsoft.SqlServer.Management.Common.DatabaseEngineType.Standalone;

			string targetFolder = String.Empty;

			#region Tables
			if (cmdlineoptions.Tables)
			{
				Console.WriteLine("Target Folder: {0}", scriptRoot);
				// create the folder structure
				if (!Directory.Exists(scriptRoot))
				{
					Console.WriteLine("Target folder does not exist - exiting");
					return;
				}

				Console.WriteLine("************** Tables *************");
				targetFolder = initTarget(scriptRoot, "Tables", cmdlineoptions);

				foreach (Table tbl in db.Tables)
				{

					// leave out tables whose names begin _ This is scope to create local tables , used as part of data imports etc
					if (!tbl.Name.StartsWith("_"))
					{
						Console.WriteLine(tbl.Name);
						StringBuilder sb = new StringBuilder();

						string fname = String.Format("{0}\\{1}.{2}.Table.sql", targetFolder, tbl.Schema, tbl.Name);
						options.FileName = fname;
						StringCollection coll = tbl.Script(options);
						//sb.Append(String.Format("-- TABLE: {0}.{1}", tbl.Schema, tbl.Name));
						//sb.Append(Environment.NewLine);
						writeFile(fname, sb, coll);
					}
				}
			}
			#endregion

			#region Views
			if (cmdlineoptions.Views)
			{
				Console.WriteLine("************** Views *************");
				targetFolder = initTarget(scriptRoot, "Views", cmdlineoptions);
				foreach (View vw in db.Views)
				{
					if ((!vw.IsSystemObject) &&
						(cmdlineoptions.IncludeTemp || !(vw.Name.ToLower().StartsWith("temp"))))
					{
						Console.WriteLine(vw.Name);

						StringBuilder sb = new StringBuilder();
						StringCollection coll = vw.Script(options);

						string str = coll[2];
						bool needsFix = false;
						if (hasWhiteSpace(str))
						{
							Console.WriteLine("Cleaning {0}...", vw.Name);
							str = lintWhiteSpace(str);
							needsFix = true;
						}
						if (isMissingNameBrackets(str, vw))
						{
							Console.WriteLine("Adding name brackets {0}.{1}...", vw.Schema, vw.Name);
							str = fixMissingNameBrackets(str, vw);
							needsFix = true;
						}

						if (needsFix)
						{
							coll[2] = str;
							str = makeALTER(str, "VIEW");
							db.ExecuteNonQuery(str);
						}
						string fname = String.Format("{0}\\{1}.{2}.View.sql", targetFolder, vw.Schema, vw.Name);
						writeFile(fname, sb, coll);
					}
				}
			}
			#endregion


			//#region Triggers
			//Console.WriteLine("************** Triggers *************");
			//targetFolder = initTarget(scriptRoot, "Triggers");
			//foreach (Trigger tr in db.Triggers)
			//{
			//    if (!tr.IsSystemObject)
			//    {
			//        Console.WriteLine(tr.Name);

			//        StringBuilder sb = new StringBuilder();
			//        StringCollection coll = tr.Script(options);

			//        string str = coll[2];
			//        bool needsFix = false;
			//        if (hasWhiteSpace(str))
			//        {
			//            Console.WriteLine("Cleaning {0}...", tr.Name);
			//            str = lintWhiteSpace(str);
			//            needsFix = true;
			//        }
			//        if (isMissingNameBrackets(str, tr))
			//        {
			//            Console.WriteLine("Adding name brackets {0}.{1}...", tr.Schema, tr.Name);
			//            str = fixMissingNameBrackets(str, tr);
			//            needsFix = true;
			//        }

			//        if (needsFix)
			//        {
			//            coll[2] = str;
			//            str = makeALTER(str, "TRIGGER");
			//            db.ExecuteNonQuery(str);
			//        }
			//        string fname = String.Format("{0}\\{1}.{2}.Trigger.sql", targetFolder, tr.IsSchemaBound, tr.Name);
			//        writeFile(fname, sb, coll);
			//    }
			//}
			//#endregion
			#region Synonyms
			Console.WriteLine("************** Synonyms *************");
			targetFolder = initTarget(scriptRoot, "Synonyms", cmdlineoptions);
			foreach (Synonym syn in db.Synonyms)
			{

				Console.WriteLine(syn.Name);
				StringBuilder sb = new StringBuilder();
				StringCollection coll = syn.Script(options);
				string fname = String.Format("{0}\\{1}.{2}.Synonym.sql", targetFolder, syn.Schema, syn.Name);
				writeFile(fname, sb, coll);
			}

			// don't use these but for completeness
			targetFolder = initTarget(scriptRoot, "Security\\Roles\\Application Roles", cmdlineoptions);
			foreach (ApplicationRole role in db.ApplicationRoles)
			{
				Console.WriteLine(role.Name);
				if (!role.Name.StartsWith("db_"))
				{
					StringBuilder sb = new StringBuilder();
					StringCollection coll = role.Script(options);
					string fname = String.Format("{0}\\{1}.Role.sql", targetFolder, role.Name);
					writeFile(fname, sb, coll);
				}
			}

			#endregion
			#region Stored Procs
			if (cmdlineoptions.StoredProcs)
			{
				Console.WriteLine("************** Stored Procedures *************");
				targetFolder = initTarget(scriptRoot, "Programmability\\Stored Procedures", cmdlineoptions);
				foreach (StoredProcedure sp in db.StoredProcedures)
				{
					if ((!sp.IsSystemObject) &&
						(cmdlineoptions.IncludeTemp || !(sp.Name.ToLower().StartsWith("temp"))))
					{
						Console.WriteLine(sp.Name);

						StringBuilder sb = new StringBuilder();
						StringCollection coll = sp.Script(options);

						bool needsFix = false;
						string str = coll[2];
						if (hasWhiteSpace(str))
						{
							Console.WriteLine("Cleaning white space{0}...", sp.Name);
							str = lintWhiteSpace(str);
							needsFix = true;
						}
						if (isMissingNameBrackets(str, sp))
						{
							Console.WriteLine("Adding name brackets {0}.{1}...", sp.Schema, sp.Name);
							str = fixMissingNameBrackets(str, sp);
							needsFix = true;
						}
						coll[2] = str;
						if (needsFix)
						{
							str = makeALTER(str, "PROCEDURE");
							try
							{
								db.ExecuteNonQuery(str);
							}
							catch (Exception ex)
							{
								Console.WriteLine("Could not modify PROCEDURE {0}.{1}- {2}", sp.Schema, sp.Name, ex.Message);
							}

						}

						string fname = String.Format("{0}\\{1}.{2}.StoredProcedure.sql", targetFolder, sp.Schema, sp.Name);
						writeFile(fname, sb, coll);

					}
				}
			}
			#endregion

			#region Functions
			if (cmdlineoptions.Functions)
			{
				Console.WriteLine("************** Functions *************");
				initTarget(scriptRoot, "Programmability\\Functions\\Table-valued Functions", cmdlineoptions);
				initTarget(scriptRoot, "Programmability\\Functions\\Scalar-valued Functions", cmdlineoptions);
				initTarget(scriptRoot, "Programmability\\Functions\\Aggregate Functions", cmdlineoptions);
				foreach (UserDefinedFunction udf in db.UserDefinedFunctions)
				{
					if (!udf.IsSystemObject)
					{
						Console.WriteLine(udf.Name);
						StringBuilder sb = new StringBuilder();
						StringCollection coll = udf.Script(options);

						bool needsFix = false;
						string str = coll[2];
						if (hasWhiteSpace(str))
						{
							Console.WriteLine("Cleaning white space{0}...", udf.Name);
							str = lintWhiteSpace(str);
							needsFix = true;
						}
						if (isMissingNameBrackets(str, udf))
						{
							Console.WriteLine("Adding name brackets {0}.{1}...", udf.Schema, udf.Name);
							str = fixMissingNameBrackets(str, udf);
							needsFix = true;
						}
						coll[2] = str;
						if (needsFix)
						{
							str = makeALTER(str, "FUNCTION");
							try
							{
								db.ExecuteNonQuery(str);
							}
							catch (Exception ex)
							{
								Console.WriteLine("Could not modify FUNCTION {0}.{1}- {2}", udf.Schema, udf.Name, ex.Message);
							}
						}

						string functiontype = String.Empty;
						switch (udf.FunctionType)
						{
							case UserDefinedFunctionType.Inline:
							case UserDefinedFunctionType.Table:
								functiontype = "Table-valued Functions";
								break;
							case UserDefinedFunctionType.Scalar:
								functiontype = "Scalar-valued Functions";
								break;
							case UserDefinedFunctionType.Unknown:
								functiontype = "Aggregate Functions";   //?? don;t use these anyway
								break;
						}


						string fname = String.Format("{0}\\programmability\\functions\\{3}\\{1}.{2}.UserDefinedFunction.sql"
							, scriptRoot, udf.Schema, udf.Name, functiontype);
						writeFile(fname, sb, coll);
					}
				}
			}
			#endregion

			#region Roles
			if (cmdlineoptions.All)
			{
				Console.WriteLine("************** Roles *************");
				targetFolder = initTarget(scriptRoot, "Security\\Roles\\Database Roles", cmdlineoptions);
				foreach (DatabaseRole role in db.Roles)
				{

					if (!role.Name.StartsWith("db_"))
					{
						Console.WriteLine(role.Name);
						StringBuilder sb = new StringBuilder();
						StringCollection coll = role.Script(options);
						string fname = String.Format("{0}\\{1}.Role.sql", targetFolder, role.Name);
						writeFile(fname, sb, coll);
					}
				}

				// don't use these but for completeness
				targetFolder = initTarget(scriptRoot, "Security\\Roles\\Application Roles", cmdlineoptions);
				foreach (ApplicationRole role in db.ApplicationRoles)
				{
					Console.WriteLine(role.Name);
					if (!role.Name.StartsWith("db_"))
					{
						StringBuilder sb = new StringBuilder();
						StringCollection coll = role.Script(options);
						string fname = String.Format("{0}\\{1}.Role.sql", targetFolder, role.Name);
						writeFile(fname, sb, coll);
					}
				}
			}
			#endregion

			#region Schemas
			if (cmdlineoptions.All)
			{

				Console.WriteLine("************** Schemas *************");
				targetFolder = initTarget(scriptRoot, "Security\\Schemas", cmdlineoptions);
				foreach (Schema sch in db.Schemas)
				{
					if (!sch.IsSystemObject)
					{

						Console.WriteLine(sch.Name);

						StringBuilder sb = new StringBuilder();
						StringCollection coll = sch.Script(options);
						string fname = String.Format("{0}\\{1}.Schema.sql", targetFolder, sch.Name);
						writeFile(fname, sb, coll);
					}
				}
			}
			#endregion
		}

		//public static void writeFile(string fname, StringBuilder sb, StringCollection coll)
		//{
		//    bool hasContent = false;
		//    bool goBefore = false;
		//    bool goAfter = false;
		//    foreach (string s in coll)
		//    {
		//        string str = stripParams(s);
		//        /// <summary>
		//        /// Not all CREATE statements need to be the first in their batch
		//        /// In particular CREATE TABLE and related statements do not need to be preceded by GO
		//        /// https://technet.microsoft.com/en-us/library/ms175502(v=sql.105).aspx
		//        /// Leaveing out excess GO statements makes the script a bit cleaner
		//        goBefore = needsGoWrapper(str);
		//        if ((goBefore || goAfter) && hasContent)
		//        {
		//            sb.Append("GO");
		//            sb.Append(Environment.NewLine);
		//        }

		//        sb.Append(str);
		//        sb.Append(Environment.NewLine);
		//        goAfter = goBefore;
		//        goBefore = false;
		//        hasContent = true;
		//    }
		//    writeStringBuilder(fname, sb);
		//}

		public static void writeFile(string fname, StringBuilder sb, StringCollection coll)
		{
			foreach (string s in coll)
			{
				string str = stripParams(s);
				sb.Append(str);
				// don't double blank lines
				if (!str.EndsWith("\r\n"))
					sb.Append(Environment.NewLine);
				sb.Append("GO");
				sb.Append(Environment.NewLine);
			}
			sb.Append(Environment.NewLine);     // end with a blank line
			writeStringBuilder(fname, sb);
		}

		public static void writeStringBuilder(string fname, StringBuilder sb)
		{
			System.IO.StreamWriter fs = System.IO.File.CreateText(fname);
			fs.Write(sb.ToString());
			fs.Close();
		}

		private static string stripParams(string str)
		{
			const string Tag = "==Scripting Parameters==";

			string[] splitter = new string[1] { "\r\n" };
			if (str.Contains(Tag))
			{
				string[] bits = str.Split(splitter, StringSplitOptions.None);
				return bits.Last();
				//return str.Replace("\r\n*/\r\n", "\r\n*/");
			}
			return str;
		}

		private static bool hasWhiteSpace(string str)
		{
			if (str.StartsWith("\r\n"))
			{
				return true;
			}
			// TEMP FIX TO AVOID A LOT OF false positives on nonclustered inde def
			if (str.Contains("PRIMARY KEY NONCLUSTERED \r\n"))
			{
				return false;
			}
			if (str.Contains("\t\r\n") || str.Contains(" \r\n"))
			{
				return true;

			}
			// no more than 2 consecutive blank lines
			if (str.Contains("\r\n\r\n\r\n\r\n"))
			{
				return true;
			}
			if (str.EndsWith("\r\n\r\n"))
			{
				return true;

			}
			return false;
		}
		private static string lintWhiteSpace(string str)
		{

			while (str.StartsWith("\r\n"))
			{
				str = str.Substring(2);
			}
			while (str.Contains("\t\r\n") || str.Contains(" \r\n"))
			{
				str = str.Replace("\t\r\n", "\r\n").Replace(" \r\n", "\r\n");
			}
			// no more than 2 consecutive blank lines
			while (str.Contains("\r\n\r\n\r\n\r\n"))
			{
				str = str.Replace("\r\n\r\n\r\n\r\n", "\r\n\r\n\r\n");
			}
			while (str.EndsWith("\r\n\r\n"))
			{
				str = str.Substring(0, str.Length - 2);
			}
			return str;
		}
		private static bool isMissingNameBrackets(string str, ScriptSchemaObjectBase o)
		{
			string[] splitter = new string[1] { "\r\n" };
			string[] lines = str.Split(splitter, StringSplitOptions.None);
			foreach (string l in lines)
			{
				if (l.ToUpper().StartsWith("CREATE"))
				{
					if (l.Contains(o.Schema + ".") || l.Contains("." + o.Name))
					{
						return true;
					}
				}
			}
			return false;
		}
		private static string fixMissingNameBrackets(string str, ScriptSchemaObjectBase o)
		{
			string[] splitter = new string[1] { "\r\n" };
			string[] lines = str.Split(splitter, StringSplitOptions.None);
			for (int i = 0; i < lines.Length; i++)
			{
				string s = lines[i];
				if (s.ToUpper().StartsWith("CREATE"))
				{
					if (s.Contains(o.Schema + ".") || s.Contains("." + o.Name))
					{
						s = s.Replace(o.Schema + ".", "[" + o.Schema + "].");
						s = s.Replace("." + o.Name, ".[" + o.Name + "]");
						lines[i] = s;
						str = string.Join("\r\n", lines);
					}
					return str;
				}
			}
			return str;
		}

		private static string makeALTER(string str, string objectType)
		{
			str = str.Replace("create " + objectType.ToLower(), "ALTER " + objectType.ToUpper());
			str = str.Replace("create " + objectType.ToUpper(), "ALTER " + objectType.ToUpper());
			str = str.Replace("Create " + objectType.ToLower(), "ALTER " + objectType.ToUpper());
			str = str.Replace("Create " + objectType.ToUpper(), "ALTER " + objectType.ToUpper());
			str = str.Replace("CREATE " + objectType.ToLower(), "ALTER " + objectType.ToUpper());
			str = str.Replace("CREATE " + objectType.ToUpper(), "ALTER " + objectType.ToUpper());

			str = str.Replace("Create View", "ALTER VIEW");
			str = str.Replace("CREATE View", "ALTER VIEW");
			while (str.Contains("\r\n\r\nALTER " + objectType.ToUpper()))
			{
				str = str.Replace("\r\n\r\nALTER " + objectType.ToUpper(), "\r\nALTER " + objectType.ToUpper());
			}
			return str;
		}
		private static bool needsGoWrapper(string str)
		{
			if (str.StartsWith("CREATE DEFAULT")
				|| str.StartsWith("CREATE FUNCTION")
				|| str.StartsWith("CREATE PROCEDURE")
				|| str.StartsWith("CREATE RULE")
				|| str.StartsWith("CREATE SCHEMA")
				|| str.StartsWith("CREATE TRIGGER")
				|| str.StartsWith("CREATE VIEW")
				|| str.Contains("\r\nCREATE DEFAULT")
				|| str.Contains("\r\nCREATE FUNCTION")
				|| str.Contains("\r\nCREATE PROCEDURE")
				|| str.Contains("\r\nCREATE RULE")
				|| str.Contains("\r\nCREATE SCHEMA")
				|| str.Contains("\r\nCREATE TRIGGER")
				|| str.Contains("\r\nCREATE VIEW")
				)
				return true;
			return false;
		}


		private static string initTarget(string scriptRoot, string target, Options cmdlineoptions)
		{
			string path = Path.Combine(scriptRoot, target);
			if (Directory.Exists(path))
			{
				string[] filePaths = Directory.GetFiles(path);
				foreach (string filePath in filePaths
					.Where(filename => (cmdlineoptions.IncludeTemp || (!filename.ToLower().StartsWith("temp")))))
				{
					File.Delete(filePath);
				}
			}
			else
			{
				Directory.CreateDirectory(path);
			}
			return path;
		}
	}
}
