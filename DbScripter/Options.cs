﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine;

namespace DbScripter
{
	class Options
	{
		public Options()
		{
			// set any defaults
			All = true;
			IncludeTemp = false;
		}


		[Option('p', "sp", HelpText = "load stored procs")]
		public bool StoredProcs { get; set; }

		[Option('t', "tables", HelpText = "load tables")]
		public bool Tables { get; set; }

		[Option('v', "views", HelpText = "load views")]
		public bool Views { get; set; }

		[Option('f', "fn", HelpText = "load functions")]
		public bool Functions { get; set; }


		[Option("all", HelpText = "script all object types")]
		public bool All { get; set; }

		[Option("includetemp", HelpText = "exclude objects named Temp<xxx>")]
		public bool IncludeTemp { get; set; }

	}
}
